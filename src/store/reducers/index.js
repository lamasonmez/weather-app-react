//for immmutable update pattern
import produce from 'immer';
import { ADD_CITY ,REMOVE_CITY} from "../constants/action-types";

const initialState = {
  cities: []
};

function rootReducer(state = initialState, action) {
  switch(action.type){
    case ADD_CITY:
      return addCity(state, action.payload);
    case REMOVE_CITY:
      return removeCity(state,action.payload);
    default:
        return state;
  }
}

/* Aux methods */
const findIndexById = (store, id) => {
  return store.findIndex(element => element.id === id);
};

const existsById = (store, id) => {
  return findIndexById(store, id) !== -1;
};
//

// Reducer Methods
/** Add City */
const addCity = (state, newCity) => {
  if (!newCity || existsById(state.cities, newCity.id)) return state;

  return produce(state, (draftState) => {
    draftState.cities.push(newCity);
  });
};

/**Remove City */
const removeCity = (state, city) => {
  if (!city) return state;

  const index = findIndexById(state.cities, city.id);

  if (index === -1) return state;

  return produce(state, (draftState) => {
    draftState.cities.splice(index, 1);
  });
};
// End of Reducer Methods
export default rootReducer;