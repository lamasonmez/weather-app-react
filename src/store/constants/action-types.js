/*
Declared Action Types As Constants
Since strings are prone to typos and duplicates it’s better to have action types declared as constants.
*/


export const ADD_CITY = 'ADD_CITY';
export const REMOVE_CITY = 'REMOVE_CITY';
export const ERROR = 'ERROR';

