import React,{useState , useEffect} from 'react';
import './cities-row.scss';
import City from './City/city';




const CitiesRow = props => {
  const [cities , setCitiesState] = useState(props.cities);

  //if (componentDidUpdate & (props.cities changed))
  useEffect(() => {
    setCitiesState(props.cities);
  },[props.cityName,props.cities]);
  return (
    <div className='cities-row justify-content-center row'>
      {cities.map((item) => (
        <div className="col">
          <City key={item.weather.id} weather={item.weather} removeCity={props.removeCity} interval={item.interval} />
        </div>
      ))}
    </div>
  )
 
};



export default CitiesRow;
