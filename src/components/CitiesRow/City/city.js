import OpenWeather from '../../../store/api/openweather';
import axios from 'axios';
import React,{useState } from 'react';
import rainy6 from '../../../assets/amcharts_weather_icons/animated/rainy-6.svg';
import './city.scss';

import RemoveButton from './remove-button';

const  City = (props) => {
const [city,setCity] = useState(props.weather);
const [interval,setInterval] = useState(props.interval);


let timerId = setTimeout(function tick() {
  //refresh weather data
  var url =  OpenWeather.getUrl({q:city.name});
  axios.get(url).then(res => {
     setCity(res.data);
      }).catch(e=>{
        console.log(e);
      });

  //
  timerId = setTimeout(tick, interval); // (*)
}, interval);





return (
    
    <div className='city-card'>
      <RemoveButton removeCity={props.removeCity} city={city}/>
      <div className='city-image-container'>
        <img src={'http://openweathermap.org/img/wn/' + city.weather[0].icon + '@2x.png'} alt={city.weather[0].main}/>
        <i className="icon-city fas fa-building"></i>
      </div>
      <div className='city-info-container'>
      <h4 className="city-name">{city.name}</h4>
      <div className="city-info">
        <div className="temprature">
          <span className="value">{city.main.temp}</span>
          <span className="unit">°C</span>
        </div>
        <span className="status">{city.weather[0].description}</span>
      </div>
      </div>
    </div>
  )
}

export default City;
