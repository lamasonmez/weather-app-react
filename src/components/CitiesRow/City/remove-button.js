import React, { useState } from "react";
import './remove-button.scss';

const RemoveButton = (props) => {
  const [city,setCityValue]=useState(props.city); //local state
  

  const removeCityFromList = (e) => {
    e.preventDefault();
    props.removeCity(city);
  }


  return (
    <button onClick={removeCityFromList} className='remove-button'><i className='fas fa-times icon-remove'></i></button>
  )
}

export default RemoveButton;
