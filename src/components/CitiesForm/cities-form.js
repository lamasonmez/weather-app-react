import React, { useState } from "react";
import './cities-form.scss';

const CitiesForm = (props) =>{

  const [city,setCityValue]=useState(""); //local state
  const [interval,setIntervalValue]=useState(1800); // local state

  const handleCityInputChanges = (e) => {
    setCityValue(e.target.value);
  }
  const handleIntervalInputChanges = (e) => {
    setIntervalValue(e.target.value);
  }
  
  const resetCityInputField = () => {
    setCityValue("")
  }
  const callAddCityFunction = (e) => {
    e.preventDefault();
    props.addCity(city,interval);
    resetCityInputField();
  }


  return (
    <div className="cities-form-row row">
         <div className="col-12">
             <form className="cities-form-card" onSubmit={callAddCityFunction}>
                 <div className="row w-100">
                         <div className="col-md-5">
                             <div className="form-group">
                                 <label className="label-form" htmlFor="name">City Name :</label>
                                 <input className="form-control" id="name"  type="text" value={city} onChange={handleCityInputChanges}></input>
                             </div>
                         </div>
                         <div className="col-md-5">
                             <div className="form-group">
                                 <label className="label-form" >Interval </label>
                                 <select  className="form-control"  value={interval} onChange={handleIntervalInputChanges}>
                                     <option  value="1800">0.5 Hour</option>
                                     <option value="3600">1 Hours</option>
                                     <option value="5400">1.5 Hours</option>
                                     <option value="7200">2 Hours</option>
                                 </select>
                             </div>
                         </div>
                         <div className="col-md-2">
                             <div className="form-group" style={{  justifyContent:'center'}}>
                                 <button type="submit" className="btn btn-primary">Add City</button>
                             </div>
                         </div>
                 </div>
             </form>
         </div>
    </div>
   )

}

export default CitiesForm;
