import axios from 'axios';
import React, { useState ,useEffect} from "react";
import './home.scss';

import CitiesRow from '../CitiesRow/cities-row'
import CitiesForm from '../CitiesForm/cities-form'
import OpenWeather from '../../store/api/openweather'

const  Home = (props) => {
  const [cities, setCitiesState] = useState([]);
  const [cityName, setCityname] = useState([]);

  const addCity =  (city,interval) => {
    console.log("interval = "+interval);
   var url =  OpenWeather.getUrl({q:city});
   axios.get(url).then(res => {
      var citiesList = cities;
      var weather = res.data;
      var item={weather,interval}
      citiesList.push(item);
      setCityname(weather.name);
      setCitiesState(citiesList);
      console.log(cities);
       }).catch(e=>{
         console.log(e);
       });

  }

  const removeCity = (city)=>{
    var citiesList = cities;
    var filteredList = citiesList.filter(obj=>obj.weather.name != city.name);
    setCitiesState(filteredList);
  }
 
  

    
  return (
    <div className='section'>
        <div className="container">
            <CitiesRow cities={cities} citName={cityName} removeCity={removeCity}/>
        </div>
        <div className="container">
            <CitiesForm addCity={addCity}/>
        </div>
    </div>
  )
}

export default Home;
