import React from 'react';
import './header.scss';

function Header() {
  return (
  
     <header className="header d-flex">
        <div className="container"> 
            <div className="row">
                <div className="col">
                    <div className="header-logo">
                        <span className="header-icon fab fa-cloudversify"></span>
                        <span className="header-title">Weather React</span>
                    </div>
                </div>
            </div>
        </div>
   </header>
  );
}

export default Header;