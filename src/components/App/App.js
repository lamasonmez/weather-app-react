import React from 'react';
import './App.scss';


import Header from '../Header/header'
import Home from '../Home/home'

function App() {
  return (
    <div className="App">
      <Header/>
      <Home/>
    </div>
  );
}

export default App;
